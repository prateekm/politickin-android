1. Project Members

	Prateek Maheshwari						pm23497
	Amber Cooper							alc959
	Enam R. Ayivi							aea787

2. Usage Instructions
 
	Politickin is an app for finding information about congressmen. On the home screen, you can search for a person by their first name,  last name or nickname*, or browse a list of all congressmen.

	On the politicians list, reached via the search bar or the browse all button, you can click on a person’s entry to view more information about them.

	On the politicians list, you can also search for another person by typing in the search bar at the bottom*. On the right side of the bar is a spinner that lets you choose whether you want to search by name, or by state. If you choose to search by state, use the state code (i.e., TX instead of Texas) for now. This will list all congressmen for the state. The list updates dynamically as you search.

	Clicking on a person in the list will bring you to their info screens. There are four kinds of information available for now. You can swipe left and right to cycle through the screens.

	The first screen gives you biographical and contact information about the person. You can click on the phone number in the contact information to open the dialer, or tap on one of the other links to open the link in your browser.

	The second screen gives you information (for now as raw data) about their legislative activity in the house or the senate.

	The third and fourth screens contain information about their election finances in the previous election year.

	Press the back button on your phone  at any time to return to the previous screen.

	* Can’t search with full name yet. Also, no leading or trailing spaces in search terms.

3. The following features have been completed:

	Database creation and importing.
	Accessing data over network (not required and used for now)
	Searching for a person by name and state, browsing, and filtering search results.
	Actionable links to additional biographical  information and contact details.
	Fetching information from database and dynamically creating layout elements.
	Gestures and swipe navigation.

4. The following features have NOT been completed:

	Auto-complete in search boxes.
	Finding politicians by user’s GPS location or using a map.
	Better presentation of data including charts and graphs.
	Sounds and vibration feedback while browsing.
	Menu items. An “About” and “Data Sources” screen and Sound and Vibration options.

5. The following features have been added that were not planned for alpha release:

	Using a database instead of network access (both functionalities exist for now).

6. Classes and major chunks of code obtained from other sources:

	DatabaseAdapter/DatabaseHelper: 
		http://stackoverflow.com/questions/9109438/how-to-use-an-existing-database-with-an-android-application
	NetworkHelper: 
		http://developer.android.com/training/basics/network-ops/connecting.html

7. Classes and major chunks of code we completed ourselves:

	SearchResults, CongressmanInfo, InfoBiography, InfoIndustry, InfoLegislative, InfoOrganizations, MainActivity.
	All layouts in res/layout
	Miscellaneous scripts for downloading, processing and importing data into database. Not included with project.

8. Known bugs/refinements/questions to be fixed for beta:

	Polishing data presentation, especially with charts and graphs.
	Using native social media apps for contact information instead of links where possible, 
	Handling missing/null values in data gracefully.
	Full name/state searches.
	Should search in the results list filter existing list or trigger a new search?
	Include filtering by house/senate, and party?
