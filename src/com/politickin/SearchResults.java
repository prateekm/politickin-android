package com.politickin;

import android.app.ActionBar;
import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

/**
 * Created by User: prateekm
 * Date: 10/21/12 at Time: 6:57 PM
 */

public class SearchResults extends ListActivity {
    private DatabaseHelper dbHelper;
    private SQLiteDatabase database;
    private CursorAdapter adapter;
    private FilterBy selectedSpinnerValue;

    private enum FilterBy {
        Name("Name"),
        State("State");

        private String filterType;

        private FilterBy(String filterType) {
            this.filterType = filterType;
        }

        @Override
        public String toString() {
            return filterType;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.results_list);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.hide();

        //get the database
        dbHelper = new DatabaseHelper(this);
        database = dbHelper.getReadableDatabase();


        if (database == null) throw new RuntimeException("null database after importing");


        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            adapter = searchWithDB(query);
            setListAdapter(adapter);
        }


        ListView listView = getListView();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent displayCongressmanInfo = new Intent("com.politickin.INFO");
                displayCongressmanInfo.putExtra("db_id", id);   // id == database primary key _id
                startActivity(displayCongressmanInfo);
            }
        });
        listView.setTextFilterEnabled(true);


        EditText results_filter = (EditText) findViewById(R.id.results_filter);
        results_filter.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });

        Spinner spinner = (Spinner) findViewById(R.id.filter_spinner);
        ArrayAdapter<FilterBy> spinner_adapter = new ArrayAdapter<FilterBy>(this, android.R.layout.simple_list_item_1, FilterBy.values());
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinner_adapter);
        selectedSpinnerValue = (FilterBy) spinner.getSelectedItem();
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                selectedSpinnerValue = (FilterBy) parent.getItemAtPosition(pos);
                EditText results_filter = (EditText) findViewById(R.id.results_filter);
                if (selectedSpinnerValue == FilterBy.State){
                    results_filter.setHint("Search By State Code (TX)");
                } else if (selectedSpinnerValue == FilterBy.Name){
                    results_filter.setHint("Search By Name");
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
                selectedSpinnerValue = FilterBy.Name;
            }
        });


    }

    Cursor getCursorForQuery(String query_in, FilterBy type) {
        Cursor result;
        String query = query_in.trim();
        //default to search by name
        if (type == null) type = FilterBy.Name;

        switch (type) {
            case Name:
                //TODO HIGH prateekm: take care of full name queries: "paul ryan" instead of "paul"
                //add a database column for full name
                result = database.query("congressmen", null, "first_name LIKE '%" + query +
                        "%' OR last_name LIKE '%" + query +
                        "%' OR nickname LIKE '%" + query + "%'", null, null, null, null, null);
                break;
            case State:
                //add a database column for full state name
                result = database.query("congressmen", null, "state LIKE '%" + query +
                        "%'", null, null, null, null, null);
                break;
            default:
                result = null;
        }
        return result;
    }

    CursorAdapter searchWithDB(String query) {
        Cursor result = getCursorForQuery(query, selectedSpinnerValue);
        if (result == null) throw new RuntimeException("Query result is null");

        adapter = new CustomCursorAdapter(this, result);

        adapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence constraint) {
                String query = "";
                if (constraint != null) query = constraint.toString();
                Cursor result = getCursorForQuery(query, selectedSpinnerValue);

                if (result == null) throw new RuntimeException("Query result is null");
                if (result.getCount() == 0) {
                    Toast toast = Toast.makeText(getApplicationContext(), "No Results Found", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 100);
                    toast.show();
                }

                return result;
            }
        });

        if (adapter.getCount() == 0) {
            Toast toast = Toast.makeText(getApplicationContext(), "No Results Found", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 100);
            toast.show();
        }

        return adapter;
    }

    @SuppressWarnings("deprecation")
    private class CustomCursorAdapter extends CursorAdapter {
        final LayoutInflater layoutInflater;
        final Cursor cursor;

        public CustomCursorAdapter(Context context, Cursor c) {
            super(context, c);
            cursor = c;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            ImageView headshot = (ImageView) view.findViewById(R.id.list_person_image);

            TextView nameEntry = (TextView) view.findViewById(R.id.name_entry);
            TextView subtitleEntry = (TextView) view.findViewById(R.id.subtitle_entry);

            String bioguide_id = cursor.getString(cursor.getColumnIndex("bioguide_id")).toLowerCase();
            String title = cursor.getString(cursor.getColumnIndex("title")) + ".";
            String first_name = cursor.getString(cursor.getColumnIndex("first_name"));
            String middle_name = cursor.getString(cursor.getColumnIndex("middle_name"));
            String last_name = cursor.getString(cursor.getColumnIndex("last_name"));
            String name_suffix = cursor.getString(cursor.getColumnIndex("name_suffix"));
            String name = title + " " + first_name + " " + middle_name + " " + last_name + " " + name_suffix;
            nameEntry.setText(name);

            char party_letter = cursor.getString(cursor.getColumnIndex("party")).charAt(0);
            String state = cursor.getString(cursor.getColumnIndex("state"));
            String district = cursor.getString(cursor.getColumnIndex("district"));

            String party;
            switch (party_letter) {
                case 'R':
                    party = "Republican";
                    view.setBackgroundColor(Color.parseColor("#8C1620"));
                    //                    view.getBackground().setAlpha(70);
                    nameEntry.setTextColor(Color.parseColor("#F2F2F2"));
                    subtitleEntry.setTextColor(Color.parseColor("#F1F1F1"));
                    //                    nameEntry.setAlpha(100);
                    //                    subtitleEntry.setAlpha(100);
                    break;
                case 'D':
                    party = "Democrat";
                    view.setBackgroundColor(Color.parseColor("#1E2552"));
                    //                    view.getBackground().setAlpha(70);
                    nameEntry.setTextColor(Color.parseColor("#F2F2F2"));
                    subtitleEntry.setTextColor(Color.parseColor("#F1F1F1"));
                    //                    nameEntry.setAlpha(100);
                    //                    subtitleEntry.setAlpha(100);
                    break;
                case 'I':
                    party = "Independent";
                    view.setBackgroundColor(Color.GRAY);
                    break;
                default:
                    party = "Independent";
                    break;
            }

            try {
                Drawable drawable = getResources().getDrawable(getResources().getIdentifier(bioguide_id, "drawable", getPackageName()));
                headshot.setImageDrawable(drawable);
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
            subtitleEntry.setText(party + " " + state + " " + district);

        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return layoutInflater.inflate(R.layout.results_list_entry, parent, false);
        }
    }
}
