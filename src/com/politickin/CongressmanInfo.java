package com.politickin;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

/**
 * Created by User: prateekm on
 * Date: 10/29/12 at Time: 9:18 PM
 */
public class CongressmanInfo extends FragmentActivity implements ActionBar.TabListener {

    private InfoSectionsPagerAdapter mInfoSectionsPagerAdapter;
    private ViewPager mViewPager;

    private DatabaseHelper databaseHelper;
    private Cursor result;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_main);

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        long id = (Long) extras.get("db_id");

        //create and populate database from seed data if it doesn't already exist
        databaseHelper = new DatabaseHelper(this);
        result = databaseHelper.searchWithDB(id);

        // Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
        mInfoSectionsPagerAdapter = new InfoSectionsPagerAdapter(getSupportFragmentManager());

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();

        // Specify that the Home/Up button should not be enabled, since there is no hierarchical
        // parent.
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);

        // Specify that we will be displaying tabs in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Set up the ViewPager, attaching the adapter and setting up a listener for when the
        // user swipes between sections.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mInfoSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(int position) {
                // When swiping between different app sections, select the corresponding tab.
                // We can also use ActionBar.Tab#select() to do this if we have a reference to the
                // Tab.
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mInfoSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by the adapter.
            // Also specify this Activity object, which implements the TabListener interface, as the
            // listener for when this tab is selected.
            actionBar.addTab(actionBar.newTab().setText(mInfoSectionsPagerAdapter.getPageTitle(i)).setTabListener(this));
        }

    }

    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    public Cursor getPersonData() {
        return result;
    }

    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    public static class InfoSectionsPagerAdapter extends FragmentPagerAdapter {

        public InfoSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0:
                    fragment = new InfoBiography();
                    return fragment;
                case 1:
                    fragment = new InfoLegislative();
                    return fragment;
                case 2:
                    fragment = new InfoIndustry();
                    return fragment;
                case 3:
                    fragment = new InfoOrganizations();
                    return fragment;
                //                case 4:
                //                    fragment = new InfoCapitolwords();
                //                    return fragment;
                default:
                    // The other sections of the app are dummy placeholders.
                    fragment = new InfoBiography();
                    return fragment;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Biographical";
                case 1:
                    return "Legislative";
                case 2:
                    return "Industry Contributions";
                case 3:
                    return "Organization Contributions";
                //                case 4:
                //                    return "CapitolWords";
                default:
                    return "Biographic Information";
            }
        }
    }

}
