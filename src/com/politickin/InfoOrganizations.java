package com.politickin;

import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User: prateekm on
 * Date: 10/29/12 at Time: 11:44 PM
 */
class InfoOrganizations extends Fragment {

    private RelativeLayout rl;
    private FragmentActivity fa;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fa = super.getActivity();
        rl = (RelativeLayout) inflater.inflate(R.layout.info_org_contributions, container, false);

        Cursor result = ((CongressmanInfo) fa).getPersonData();
        setDataFields(result);

        return rl;
    }

    private void setDataFields(Cursor result) {
        result.moveToFirst();

        ImageView headshot = (ImageView) rl.findViewById(R.id.person_image);

        TextView person_name = (TextView) rl.findViewById(R.id.person_name);
        TextView person_party = (TextView) rl.findViewById(R.id.person_party);
        TextView person_place = (TextView) rl.findViewById(R.id.person_place);

        String bioguide_id = result.getString(result.getColumnIndex("bioguide_id")).toLowerCase();
        String title = result.getString(result.getColumnIndex("title")) + ".";
        String first_name = result.getString(result.getColumnIndex("first_name"));
        String middle_name = result.getString(result.getColumnIndex("middle_name"));
        String last_name = result.getString(result.getColumnIndex("last_name"));
        String name_suffix = result.getString(result.getColumnIndex("name_suffix"));
        String name = title + " " + first_name + " " + middle_name + " " + last_name + " " + name_suffix;

        char party_letter = result.getString(result.getColumnIndex("party")).charAt(0);
        String state = result.getString(result.getColumnIndex("state"));
        String district = result.getString(result.getColumnIndex("district"));

        String party;
        switch (party_letter) {
            case 'R':
                party = "Republican";
                break;
            case 'D':
                party = "Democrat";
                break;
            case 'I':
                party = "Independent";
                break;
            default:
                party = "Independent";
                break;
        }

        Drawable drawable = getResources().getDrawable(getResources().getIdentifier(bioguide_id, "drawable", fa.getApplicationContext().getPackageName()));
        headshot.setImageDrawable(drawable);

        person_name.setText(name);
        person_party.setText(party);
        person_place.setText(state + " " + district);

        String organization_contributors_s = result.getString(result.getColumnIndex("opensecrets_contributors"));
        JSONArray organization_contributors = parseStringAsJSONArray(organization_contributors_s);

        ListView list = (ListView) rl.findViewById(R.id.organization_list);

        ArrayList<HashMap<String, String>> contributorsArray = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < organization_contributors.length(); i++) {
            try {
                JSONObject jsonObject = organization_contributors.getJSONObject(i);
                HashMap<String, String> map = new HashMap<String, String>();
                String org_name = jsonObject.getString("org_name");
                String total = jsonObject.getString("total");
                map.put("org_name", org_name);
                map.put("total", total);
                contributorsArray.add(map);
            } catch (JSONException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        SimpleAdapter mSchedule = new SimpleAdapter(getActivity(), contributorsArray, R.layout.info_org_contrib_entry, new String[]{"org_name", "total"}, new int[]{R.id.org_contributor, R.id.org_amount});

        list.setAdapter(mSchedule);
    }

    private JSONArray parseStringAsJSONArray(String contentAsString) {
        JSONArray json;
        try {
            json = new JSONArray(contentAsString);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return json;
    }
}