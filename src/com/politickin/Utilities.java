package com.politickin;

import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Pair;
import android.widget.TextView;

import java.util.List;

/**
 * Created by User: prateekm on
 * Date: 11/21/12 at Time: 12:54 AM
 */
class Utilities {
    public static void setTextViewValues(List<Pair<TextView, String>> valuesList) {
        for (Pair<TextView, String> pair : valuesList) {
            TextView textView = pair.first;
            String string = pair.second;

            if (string == null || string.isEmpty() || string.equals("null") || string.equals(" ")) {
                textView.setText("Not Available");
            } else {
                textView.setText(string.trim());
            }
        }
    }

    public static void setTextViewLinks(List<Pair<TextView, Pair<String, String>>> valuesList) {
        for (Pair<TextView, Pair<String, String>> pair : valuesList) {
            String label = pair.second.first;
            String value = pair.second.second;
            TextView textView = pair.first;
            textView.setText(Html.fromHtml("<a href=\"" + value + "\">" + label + "</a>"));
            textView.setMovementMethod(LinkMovementMethod.getInstance());
            stripUnderlines(textView);
        }
    }

    public static void stripUnderlines(TextView textView) {
        Spannable s = (Spannable) textView.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span : spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        textView.setText(s);
    }
}

class URLSpanNoUnderline extends URLSpan {
    public URLSpanNoUnderline(String url) {
        super(url);
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
    }
}
