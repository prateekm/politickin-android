package com.politickin;

import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by User: prateekm on
 * Date: 10/29/12 at Time: 11:33 PM
 */
class InfoCapitolwords extends Fragment {

    private RelativeLayout rl;
    private FragmentActivity fa;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fa = super.getActivity();
        rl = (RelativeLayout) inflater.inflate(R.layout.info_capitolwords, container, false);

        Cursor result = ((CongressmanInfo) fa).getPersonData();
        setDataFields(result);

        return rl;
    }

    private void setDataFields(Cursor result) {
        result.moveToFirst();

        ImageView headshot = (ImageView) rl.findViewById(R.id.person_image);

        TextView person_name = (TextView) rl.findViewById(R.id.person_name);
        TextView person_party = (TextView) rl.findViewById(R.id.person_party);
        TextView person_place = (TextView) rl.findViewById(R.id.person_place);

        String bioguide_id = result.getString(result.getColumnIndex("bioguide_id")).toLowerCase();
        String title = result.getString(result.getColumnIndex("title")) + ".";
        String first_name = result.getString(result.getColumnIndex("first_name"));
        String middle_name = result.getString(result.getColumnIndex("middle_name"));
        String last_name = result.getString(result.getColumnIndex("last_name"));
        String name_suffix = result.getString(result.getColumnIndex("name_suffix"));
        String name = title + " " + first_name + " " + middle_name + " " + last_name + " " + name_suffix;

        char party_letter = result.getString(result.getColumnIndex("party")).charAt(0);
        String state = result.getString(result.getColumnIndex("state"));
        String district = result.getString(result.getColumnIndex("district"));

        String party;
        switch (party_letter) {
            case 'R':
                party = "Republican";
                break;
            case 'D':
                party = "Democrat";
                break;
            case 'I':
                party = "Independent";
                break;
            default:
                party = "Independent";
                break;
        }

        Drawable drawable = getResources().getDrawable(getResources().getIdentifier(bioguide_id, "drawable", fa.getApplicationContext().getPackageName()));
        headshot.setImageDrawable(drawable);

        person_name.setText(name);
        person_party.setText(party);
        person_place.setText(state + " " + district);
    }
}