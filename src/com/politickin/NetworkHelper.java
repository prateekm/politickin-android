package com.politickin;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by User: prateekm on
 * Date: 10/23/12 at Time: 8:11 PM
 */

// Uses AsyncTask to create a task away from the main UI thread. This task takes a
// URL string and uses it to create an HttpUrlConnection. Once the connection
// has been established, the AsyncTask downloads the contents of the webpage as
// an InputStream. Finally, the InputStream is converted into a string, which is
// displayed in the UI by the AsyncTask's onPostExecute method.

// Implementing classes should override onPostExecute method to deal with the results

public abstract class NetworkHelper extends AsyncTask<String, Integer, JSONObject> {

    private final ConnectivityManager connMgr;
    private final NetworkInfo networkInfo;
    private Log log;
    private static final String DEBUG_TAG = "net";

    public NetworkHelper(Context context) {
        connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();
    }

    public void searchWithNetwork(String query) {
        if (networkInfo != null && networkInfo.isConnected()) {
            execute(query);
        } else {
            log.e(DEBUG_TAG, "No network connection available.");
        }
    }

    @Override
    public JSONObject doInBackground(String... urls) {
        try {
            return parseStringAsJSON(getPageContentAsString(urls[0]));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public abstract void onPostExecute(JSONObject result);

    // Given a URL, establishes an HttpUrlConnection and retrieves
    // the web page content as a InputStream, which it returns as
    // a String.
    private String getPageContentAsString(String query) throws IOException {
        InputStream inputStream = null;
        try {
            // Set connection parameters
            URL url = new URL(query);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(60000 /* milliseconds */);
            conn.setConnectTimeout(60000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);

            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();

            // Get the InputStream and convert it into a string
            inputStream = conn.getInputStream();
            InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            String contentAsString = br.readLine();
            br.close();

            return contentAsString;

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    private JSONObject parseStringAsJSON(String contentAsString) {
        JSONObject json;
        try {
            json = new JSONObject(contentAsString);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return json;
    }
}