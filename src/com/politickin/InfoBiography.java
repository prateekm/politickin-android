package com.politickin;

import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by User: prateekm
 * Date: 10/21/12 at Time: 3:26 PM
 */

@SuppressWarnings("unchecked")
class InfoBiography extends Fragment {

    private RelativeLayout rl;
    private FragmentActivity fa;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fa = super.getActivity();
        rl = (RelativeLayout) inflater.inflate(R.layout.info_biography, container, false);

        Cursor result = ((CongressmanInfo) fa).getPersonData();
        setDataFields(result);

        return rl;
    }

    private void setDataFields(Cursor result) {
        result.moveToFirst();

        ImageView headshot = (ImageView) rl.findViewById(R.id.person_image);

        TextView person_name = (TextView) rl.findViewById(R.id.person_name);
        TextView person_party = (TextView) rl.findViewById(R.id.person_party);
        TextView person_place = (TextView) rl.findViewById(R.id.person_place);

        TextView nickname = (TextView) rl.findViewById(R.id.nickname);
        TextView birth = (TextView) rl.findViewById(R.id.birth);
        TextView religion = (TextView) rl.findViewById(R.id.religion);
        TextView education = (TextView) rl.findViewById(R.id.education);

        TextView address = (TextView) rl.findViewById(R.id.address);
        TextView phone = (TextView) rl.findViewById(R.id.phone);

        TextView website = (TextView) rl.findViewById(R.id.website);
        TextView email = (TextView) rl.findViewById(R.id.email);
        TextView facebook = (TextView) rl.findViewById(R.id.facebook);
        TextView twitter = (TextView) rl.findViewById(R.id.twitter);
        TextView youtube = (TextView) rl.findViewById(R.id.youtube);
        TextView metavid = (TextView) rl.findViewById(R.id.metavid);
        TextView congresspedia = (TextView) rl.findViewById(R.id.congresspedia);
        TextView wikipedia = (TextView) rl.findViewById(R.id.wikipedia);

        String bioguide_id = result.getString(result.getColumnIndex("bioguide_id")).toLowerCase();
        String title = result.getString(result.getColumnIndex("title")) + ".";
        String first_name = result.getString(result.getColumnIndex("first_name"));
        String middle_name = result.getString(result.getColumnIndex("middle_name"));
        String last_name = result.getString(result.getColumnIndex("last_name"));
        String name_suffix = result.getString(result.getColumnIndex("name_suffix"));
        String name = title + " " + first_name + " " + middle_name + " " + last_name + " " + name_suffix;

        char party_letter = result.getString(result.getColumnIndex("party")).charAt(0);
        String state = result.getString(result.getColumnIndex("state"));
        String district = result.getString(result.getColumnIndex("district"));

        String party;
        switch (party_letter) {
            case 'R':
                party = "Republican";
                break;
            case 'D':
                party = "Democrat";
                break;
            case 'I':
                party = "Independent";
                break;
            default:
                party = "Independent";
                break;
        }

        String birthday_s = result.getString(result.getColumnIndex("birthdate"));
        String birthplace_s = result.getString(result.getColumnIndex("birthplace"));
        StringBuilder birth_sb = new StringBuilder();
        if (!(birthday_s == null) && !birthday_s.isEmpty() && !birthday_s.equals("null")) {
            birth_sb.append(birthday_s);
        }
        if (!(birthplace_s == null) && !birthplace_s.isEmpty() && !birthplace_s.equals("null")) {
            birth_sb.append(", ").append(birthplace_s);
        }
        String birth_s = birth_sb.toString();

        String religion_s = result.getString(result.getColumnIndex("religion"));
        String nickname_s = result.getString(result.getColumnIndex("nickname"));
        String education_s = result.getString(result.getColumnIndex("education"));

        String address_s = result.getString(result.getColumnIndex("congress_address"));
        String phone_s = result.getString(result.getColumnIndex("phone"));

        String website_s = result.getString(result.getColumnIndex("website"));
        String email_s = result.getString(result.getColumnIndex("webform"));
        String facebook_s = result.getString(result.getColumnIndex("facebook_id"));
        String twitter_s = result.getString(result.getColumnIndex("twitter_id"));
        String youtube_s = result.getString(result.getColumnIndex("youtube_url"));
        String metavid_s = result.getString(result.getColumnIndex("metavid_id"));
        String congresspedia_s = result.getString(result.getColumnIndex("open_congress_url"));
        String wikipedia_s = result.getString(result.getColumnIndex("wikipedia_url"));

        Drawable drawable = getResources().getDrawable(getResources().getIdentifier(bioguide_id, "drawable", fa.getApplicationContext().getPackageName()));
        headshot.setImageDrawable(drawable);

        ArrayList<Pair<TextView, String>> textViewsValues = new ArrayList<Pair<TextView, String>>();
        textViewsValues.add(new Pair(person_name, name));
        textViewsValues.add(new Pair(person_party, party));
        textViewsValues.add(new Pair(person_place, state + " " + district));
        textViewsValues.add(new Pair(nickname, nickname_s));
        textViewsValues.add(new Pair(birth, birth_s));
        textViewsValues.add(new Pair(religion, religion_s));
        textViewsValues.add(new Pair(education, education_s));
        textViewsValues.add(new Pair(address, address_s));
        Utilities.setTextViewValues(textViewsValues);

        SpannableString phone_link = new SpannableString(phone_s);
        phone_link.setSpan(new URLSpan("tel:" + phone_s.replaceAll("-", "")), 0, phone_s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        phone.setText(phone_link);
        phone.setMovementMethod(LinkMovementMethod.getInstance());
        Utilities.stripUnderlines(phone);

        ArrayList<Pair<TextView, Pair<String, String>>> textViewsLinksValues = new ArrayList<Pair<TextView, Pair<String, String>>>();
        textViewsLinksValues.add(new Pair(website, new Pair("Website", website_s)));
        textViewsLinksValues.add(new Pair(email, new Pair("Email", email_s)));
        textViewsLinksValues.add(new Pair(congresspedia, new Pair("Congresspedia", congresspedia_s)));
        textViewsLinksValues.add(new Pair(wikipedia, new Pair("Wikipedia", wikipedia_s)));
        textViewsLinksValues.add(new Pair(facebook, new Pair("Facebook", "http://www.facebook.com/" + facebook_s)));
        textViewsLinksValues.add(new Pair(twitter, new Pair("Twitter", "http://twitter.com/" + twitter_s)));
        textViewsLinksValues.add(new Pair(youtube, new Pair("Youtube", youtube_s)));
        textViewsLinksValues.add(new Pair(metavid, new Pair("Metavid", "http://metavid.ucsc.edu//" + metavid_s)));
        Utilities.setTextViewLinks(textViewsLinksValues);
    }
}
